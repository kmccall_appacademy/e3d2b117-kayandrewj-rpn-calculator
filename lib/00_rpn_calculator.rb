class RPNCalculator
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    operate(:+)
  end

  def minus
    operate(:-)
  end

  def times
    operate(:*)
  end

  def divide
    operate(:/)
  end


  def tokens(string)
    tokens = string.split
    tokens.map do |char|
      operation?(char) ? char.to_sym : Integer(char)
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        self.push(token)
      else
        operate(token)
      end
    end
    self.value
  end

  private

  def operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end

  def operate(symbol)
    raise "calculator is empty" if @stack.size < 2

    second_num = @stack.pop
    first_num = @stack.pop

    case symbol
    when :+
      @stack << first_num + second_num
    when :-
      @stack << first_num - second_num
    when :*
      @stack << first_num * second_num
    when :/
      @stack << first_num.fdiv(second_num)
    else
      @stack << first_num
      @stack << second_num
      raise "No such operation: #{symbol}"
    end
  end
end
